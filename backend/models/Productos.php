<?php

namespace app\models;

use Yii;
use app\models\Categorias;
use app\models\Tags;
use cornernote\linkall\LinkAllBehavior;


/**
 * This is the model class for table "productos".
 *
 * @property int $id
 * @property string $name
 * @property string $description
 * @property int $categorias_id
 */
class Productos extends \yii\db\ActiveRecord
{

   public $tag_ids;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'productos';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
          [['name','tag_ids'], 'required'],
          [['categorias_id'], 'integer'],
          [['name'], 'string', 'max' => 50],
          [['description'], 'string', 'max' => 250],
      ];
  }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'description' => 'Description',
            'categorias_id' => 'Categorias ID',
     
        ];
    }



    public function getParent()
    {
        return $this->hasOne(Categorias::className(), ['id' => 'categorias_id']);
    }

    public function getParentName(){
        $model=$this->parent;
        return $model?$model->name:'';
    }



    public function behaviors()
    {
        return [
            LinkAllBehavior::className(),
        ];
    }

        public function afterSave($insert, $changedAttributes)
    {
        $tags = [];
        foreach ($this->tag_ids as $tag_name) {
            $tag = Tags::getTagByName($tag_name);
            if ($tag) {
                $tags[] = $tag;
            }
        }
        $this->linkAll('tags', $tags);
        parent::afterSave($insert, $changedAttributes);
    }

    public function getTags()
    {


        return $this->hasMany(Tags::className(), ['id' => 'tag_id'])
            //->via('postToTag');
            ->viaTable('productos_tags', ['producto_id' => 'id']);
    }

        public function getMitags()
    {

   
        return 'Douglas';
    }

}
