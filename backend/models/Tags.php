<?php

namespace app\models;

use Yii;
use app\models\Tags;

/**
 * This is the model class for table "tags".
 *
 * @property int $id
 * @property string $name
 */
class Tags extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tags';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
          [['name'], 'required'],
          [['name'], 'unique', 'targetClass' => '\app\models\Tags', 'message' => 'This tags has already been taken.'],
          [['name'], 'string', 'max' => 50],
      ];
  }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
        ];
    }



        public static function getTagByName($name)
    {
        $tag = Tags::find()->where(['name' => $name])->one();
        if (!$tag) {
            $tag = new Tag();
            $tag->name = $name;
            $tag->save(false);
        }
        return $tag;
    }
}
