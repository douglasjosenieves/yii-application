<?php
return [
   'name'=>'YII2 APP!',
   'aliases' => [
    '@bower' => '@vendor/bower-asset',
    '@npm'   => '@vendor/npm-asset',
],
'vendorPath' => dirname(dirname(__DIR__)) . '/vendor',
'components' => [
    'cache' => [
        'class' => 'yii\caching\FileCache',
    ],
],
];
